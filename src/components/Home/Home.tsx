import React, { useEffect } from 'react'
import { Link } from '@material-ui/core'
import clsx from 'clsx'
import { Link as RouterLink } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { getDaysBetweenDates } from '../../helpers/date'
import { RootState } from '../../redux/store'
import useStyles from './hooks/useStyles'

const Home = () => {
  const classes = useStyles()
  const {
    firstname,
    birthDate,
  } = useSelector((app: RootState) => app.user)

  useEffect(() => {
    document.title = 'Accueil - Test React Actual'
  }, [])

  return (
    <div className="container">
      {firstname && birthDate ? (
        <div className={clsx(classes.marginTop)}>
          <p>
            {`Bonjour ${firstname}, votre anniversaire est dans ${getDaysBetweenDates(new Date(birthDate), new Date())} jours.`}
          </p>
          <p>
            Si cela est incorrect, vous pouvez modifier les informations sur votre page
            {' '}
            <Link to="/mes-informations" component={RouterLink}>informations</Link>
            .
          </p>
        </div>
      ) : (
        <div className={clsx(classes.marginTop)}>
          Veuillez compléter vos informations sur la page
          {' '}
          <Link to="/mes-informations" component={RouterLink}>informations</Link>
          .
        </div>
      )}
    </div>
  )
}

export default Home
