import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  list: {
    width: 250,
  },
  navbar: {
    zIndex: 100,
    boxShadow: '0 5px 44px 0 rgba(172, 193, 204, 0.25)',
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'center',
  },
  menuButton: {
    marginLeft: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  currentLocation: {
    fontSize: 25,
    marginTop: -5,
  },
}))

export default useStyles
