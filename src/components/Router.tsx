import React, { ReactNode } from 'react'
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom'
import Home from './Home'
import Informations from './Informations'

interface RouterProps {
  children: ReactNode
}

const Router = ({ children }: RouterProps) => (
  <BrowserRouter>
    <div>
      {children}
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/mes-informations">
          <Informations />
        </Route>
      </Switch>
    </div>
  </BrowserRouter>
)

export default Router
