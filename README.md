# Test React Actual

## Projet
Projet réalisé en ReactJS, sur deux pages :
- une page d'accueil qui comprend un message de bienvenue
- une page d'informations contenant un formulaire dont les données alimentent le message de bienvenue de la page d'accueil

Le site a été conçu en PWA et vous pouvez donc l'ajouter en favoris sur votre écran d'accueil pour profiter de l'expérience PWA.

### Installation et démarrage
- `yarn install` pour installer les dépendances du projet
- `yarn start` pour démarrer le serveur de développement

### Commandes supplémentaires
- `yarn lint` pour parser les fichiers du dossier `src/` avec eslint