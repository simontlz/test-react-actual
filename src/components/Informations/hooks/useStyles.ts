import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  formContainer: {
    display: 'flex',
    marginTop: 30,
  },
  fixedInputWidth: {
    width: 400,
  },
  columnDirection: {
    flexDirection: 'column',
  },
  marginBottom: {
    marginBottom: 20,
  },
}))

export default useStyles
