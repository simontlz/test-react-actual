import { UserType } from '../redux/features/user/userSlice'

const BASE_URL = 'https://reqres.in/api'

export const userApiEndpoint = {
  post: (userData: UserType) => new Promise((resolve, reject) => {
    const form = new FormData()
    Object.keys(userData).forEach((userAttribute) => {
      form.append(userAttribute, (userData as any)[userAttribute])
    })

    fetch(
      `${BASE_URL}/users`,
      { method: 'POST', body: form },
    )
      .then((response) => {
        response.json()
          .then((jsonResponse) => resolve(jsonResponse))
      })
      .catch((error) => reject(error))
  }),
}

export default {}
