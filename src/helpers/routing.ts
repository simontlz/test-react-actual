export const getRouteNameByPath = (path: string) => {
  switch (path) {
    case '/mes-informations':
      return 'Mes informations'
    case '/':
    default:
      return 'Accueil'
  }
}

export default {}
