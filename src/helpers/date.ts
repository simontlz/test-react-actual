export const getDaysBetweenDates = (fromDate: Date, toDate: Date) => {
  const oneDay = 1000 * 60 * 60 * 24
  const fromDateMonth = fromDate.getMonth() + 1 < 10 ? `0${fromDate.getMonth() + 1}` : fromDate.getMonth() + 1
  const fromDateWithoutYears = new Date(`2021-${fromDateMonth}-${fromDate.getDate()}`)
  const differenceMs = Math.abs((fromDateWithoutYears as any) - (toDate as any))

  return Math.round(differenceMs / oneDay)
}

export default {}
