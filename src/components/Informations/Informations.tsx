import React, { useEffect } from 'react'
import { FormControl, TextField, useMediaQuery } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import clsx from 'clsx'
import { toast } from 'react-toastify'
import { RootState } from '../../redux/store'
import { setUserBirthDate, setUserFirstname, setUserLastname } from '../../redux/features/user/userSlice'
import useStyles from './hooks/useStyles'

const Informations = () => {
  const dispatch = useDispatch()
  const classes = useStyles()
  const isMobile = useMediaQuery('(max-width:600px)')
  const {
    firstname,
    lastname,
    birthDate,
  } = useSelector((app: RootState) => app.user)

  useEffect(() => {
    document.title = 'Mes informations - Test React Actual'
  }, [])

  return (
    <form noValidate className="container">
      <FormControl
        className={
          clsx(
            classes.formContainer,
          )
        }
      >
        <TextField
          id="firstname-input"
          label="Prénom"
          variant="outlined"
          margin="dense"
          value={firstname}
          className={clsx(classes.marginBottom, !isMobile && classes.fixedInputWidth)}
          onChange={(text) => dispatch(setUserFirstname(text.target.value))}
          onBlur={() => firstname.length && toast('Données sauvegardées !')}
        />
        <TextField
          id="lastname-input"
          label="Nom"
          variant="outlined"
          margin="dense"
          value={lastname}
          className={clsx(classes.marginBottom, !isMobile && classes.fixedInputWidth)}
          onChange={(text) => dispatch(setUserLastname(text.target.value))}
          onBlur={() => firstname.length && toast('Données sauvegardées !')}
        />
        <TextField
          id="birthdate-input"
          label="Date de naissance"
          variant="outlined"
          margin="dense"
          defaultValue={birthDate || '2021-06-18'}
          type="date"
          className={clsx(classes.marginBottom, !isMobile && classes.fixedInputWidth)}
          onChange={(text) => dispatch(setUserBirthDate(text.target.value))}
          onBlur={() => toast('Données sauvegardées !')}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </FormControl>
    </form>
  )
}

export default Informations
