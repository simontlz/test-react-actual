import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  marginTop: {
    marginTop: 30,
  },
}))

export default useStyles
