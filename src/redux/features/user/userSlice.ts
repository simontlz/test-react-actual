import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../../store'
import { userApiEndpoint } from '../../../helpers/api'

export type UserType = {
  firstname: string,
  lastname: string,
  birthDate: string,
}

const initialState: UserType = {
  firstname: '',
  lastname: '',
  birthDate: '',
}

const setUserFirstname = createAsyncThunk(
  'users/setFirstName',
  async (firstname: string, { getState }) => {
    const { user } = getState() as RootState
    userApiEndpoint.post({ ...user, firstname })

    return firstname
  },
)
const setUserLastname = createAsyncThunk(
  'users/setLastname',
  async (lastname: string, { getState }) => {
    const { user } = getState() as RootState
    userApiEndpoint.post({ ...user, lastname })

    return lastname
  },
)
const setUserBirthDate = createAsyncThunk(
  'users/setBirthDate',
  async (birthDate: string, { getState }) => {
    const { user } = getState() as RootState
    userApiEndpoint.post({ ...user, birthDate })

    return birthDate
  },
)

export const userSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(setUserFirstname.fulfilled, (state, action) => {
      state.firstname = (action.payload as unknown as string)
    })
    builder.addCase(setUserLastname.fulfilled, (state, action) => {
      state.lastname = (action.payload as unknown as string)
    })
    builder.addCase(setUserBirthDate.fulfilled, (state, action) => {
      state.birthDate = (action.payload as unknown as string)
    })
  },
})

export { setUserLastname, setUserFirstname, setUserBirthDate }
export default userSlice.reducer
