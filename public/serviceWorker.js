const CACHE_NAME = 'test-react-actual'

const urlsToCache = [
  '/',
  '/mes-informations',
]

self.addEventListener('install', (event) => {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then((cache) => {
        console.log('Opened cache')
        return cache.addAll(urlsToCache)
      }),
  )
  self.skipWaiting()
})

self.addEventListener('fetch', (event) => {
  event.respondWith(caches.match(event.request)
    .then((response) => response ?? fetch(event.request)))
})
