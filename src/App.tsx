import React from 'react'
import { Flip, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Router from './components/Router'
import Header from './components/Header'
import './App.css'

const App = () => (
  <Router>
    <ToastContainer
      transition={Flip}
      autoClose={1000}
      hideProgressBar
      closeOnClick
      limit={2}
    />
    <Header />
  </Router>
)

export default App
