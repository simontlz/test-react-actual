import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import {
  IconButton,
  List,
  SwipeableDrawer,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core'
import { Menu as MenuIcon, Home as HomeIcon, AccountBox as AccountIcon } from '@material-ui/icons'
import { useHistory } from 'react-router-dom'
import useStyle from './hooks/useStyles'
import { getRouteNameByPath } from '../../helpers/routing'

// Disabling discovery feature for iOS
const isIOS = (process as any).browser && /iPad|iPhone|iPod/.test(navigator.userAgent)

const Header = () => {
  const [isDrawerOpened, setIsDrawerOpened] = useState(false)
  const history = useHistory()
  const [currentRoute, setCurrentRoute] = useState(getRouteNameByPath(history.location.pathname))
  const classes = useStyle()

  useEffect(() => history.listen((location) => (
    setCurrentRoute(getRouteNameByPath(location.pathname))
  )), [history])

  return (
    <>
      <header>
        <nav className={clsx(classes.navbar)} role="navigation">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={() => setIsDrawerOpened(true)}
            className={clsx(classes.menuButton, isDrawerOpened && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <span className={clsx(classes.currentLocation)}>
            {currentRoute}
          </span>
        </nav>
      </header>
      <SwipeableDrawer
        anchor="left"
        open={isDrawerOpened}
        onClose={() => setIsDrawerOpened(false)}
        onOpen={() => setIsDrawerOpened(true)}
        disableDiscovery={isIOS}
      >
        <div
          className={clsx(classes.list)}
          role="presentation"
          onClick={() => setIsDrawerOpened(false)}
        >
          <List>
            <ListItem
              button
              onClick={() => history.push('/')}
            >
              <ListItemIcon><HomeIcon /></ListItemIcon>
              <ListItemText primary="Accueil" />
            </ListItem>
            <ListItem
              button
              onClick={() => history.push('/mes-informations')}
            >
              <ListItemIcon><AccountIcon /></ListItemIcon>
              <ListItemText primary="Mes informations" />
            </ListItem>
          </List>
        </div>
      </SwipeableDrawer>
    </>
  )
}

export default Header
